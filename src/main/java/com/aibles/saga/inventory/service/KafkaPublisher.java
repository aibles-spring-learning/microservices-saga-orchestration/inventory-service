package com.aibles.saga.inventory.service;

import com.leonrad.saga.dto.inventory.InventoryResponseDTO;

public interface KafkaPublisher {
    void send(InventoryResponseDTO inventoryResponseDTO);
}
