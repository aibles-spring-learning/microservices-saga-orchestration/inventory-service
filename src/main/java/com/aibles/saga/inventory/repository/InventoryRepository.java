package com.aibles.saga.inventory.repository;

import com.aibles.saga.inventory.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Integer> {
    Optional<Inventory> findByProductIdAndAmountGreaterThan(int productId, int amount);
}
