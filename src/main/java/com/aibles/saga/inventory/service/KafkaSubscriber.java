package com.aibles.saga.inventory.service;

import com.leonrad.saga.dto.inventory.InventoryRequestDTO;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Configuration
public interface KafkaSubscriber {

    @KafkaListener(
            topics = "${kafka.consumer.topic}",
            containerFactory = "${kafka.consumer.container.factory}"
    )
    void updateInventory(InventoryRequestDTO inventoryRequestDTO);
}
