package com.aibles.saga.inventory.service.iml;

import com.aibles.saga.inventory.model.Inventory;
import com.aibles.saga.inventory.repository.InventoryRepository;
import com.aibles.saga.inventory.service.KafkaPublisher;
import com.aibles.saga.inventory.service.KafkaSubscriber;
import com.leonrad.saga.dto.inventory.InventoryRequestDTO;
import com.leonrad.saga.dto.inventory.InventoryResponseDTO;
import com.leonrad.saga.dto.inventory.InventoryStatus;
import com.leonrad.saga.dto.utils.EntityMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class KafkaSubscriberIml implements KafkaSubscriber {

    private final InventoryRepository inventoryRepository;
    private final KafkaPublisher kafkaPublisher;
    private final int amountMin = 0;

    @Override
    public void updateInventory(InventoryRequestDTO inventoryRequestDTO) {
        InventoryResponseDTO inventoryResponseDTO = EntityMapper.map(inventoryRequestDTO, InventoryResponseDTO.class);
        inventoryRepository.findByProductIdAndAmountGreaterThan(inventoryRequestDTO.getProductId(), amountMin)
                .map(inventory -> updateAmountProduct(inventory))
                .map(inventory -> {
                    inventoryResponseDTO.setInventoryStatus(InventoryStatus.AVAILABLE);
                    return inventoryResponseDTO;
                })
                .orElseGet(() -> {
                    inventoryResponseDTO.setInventoryStatus(InventoryStatus.OUT_OF_STOCK);
                    return inventoryResponseDTO;
                });
        System.out.println(inventoryResponseDTO);
        kafkaPublisher.send(inventoryResponseDTO);
    }

    private Inventory updateAmountProduct(Inventory inventory){
        int currentAmount = inventory.getAmount();
        inventory.setAmount(currentAmount - 1);
        return inventoryRepository.save(inventory);
    }

}
