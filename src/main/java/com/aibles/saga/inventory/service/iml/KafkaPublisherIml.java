package com.aibles.saga.inventory.service.iml;

import com.aibles.saga.inventory.service.KafkaPublisher;
import com.leonrad.saga.dto.inventory.InventoryResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KafkaPublisherIml implements KafkaPublisher {

    private final KafkaTemplate<String, InventoryResponseDTO> kafkaTemplate;
    private final String messageKey = "key";

    @Value("${kafka.publisher.topic}")
    private String publisherTopic;


    @Override
    public void send(InventoryResponseDTO inventoryResponseDTO) {
        kafkaTemplate.send(publisherTopic, messageKey, inventoryResponseDTO);
    }
}
